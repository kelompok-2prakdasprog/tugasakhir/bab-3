Nama : Dian Hasna Ramadhani
NIM : 1207050026
Kelas : Teknik Informstika / B

BAB 3
2. Definisikan sebuah tipe terstruktur untuk menyatakan data penerbangan di sebuah  bandara. Data penerbangan terdiri atas :  nomor penerbangan (miosal : GA101), bandara (kota) asal, bandara tujuan, tanggal keberangkatan, jam keberangkatan (depature time), jam datang (arrival time). Untuk setiap field, definisikan tipe data yang cocok.
Jawab :
type Tanggal  : record < dd : integer,
			 mm : integer,
			 yy : integer
		            >
type Jam  : record < hh : integer,
		       mm : integer,
		       ss : integer
		    >
type JadwalPesawat : record <NoPenerbangan : string,
			            KotaAsal  : string,
			            BandaraTujaun : string,
			            TanggalBerangkat :  Tanggal,
			            JamBerangkat : Jam,
			            JamDatang : Jam
			          > 
